package com.example.consumeapi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.consumeapi.IntroSlider.WelcomeActivity;

public class MainActivity2 extends AppCompatActivity {
    int time  = 4000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent home=new Intent(MainActivity2.this, WelcomeActivity.class);
                startActivity(home);
                finish();

            }
        },time);
    }
}