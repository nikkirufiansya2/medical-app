package com.example.consumeapi;

import com.google.gson.annotations.SerializedName;

public class RetroUsers {
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;


    public RetroUsers(String name, String email){
        this.name = name;
        this.email = email;
    }

    public String getUser(){
        return name;
    }


    public void setUser(String name){
        this.name = name;
    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email){
        this.email = email;
    }




}
