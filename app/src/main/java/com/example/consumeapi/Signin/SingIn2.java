package com.example.consumeapi.Signin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.consumeapi.R;

import java.util.ArrayList;
import java.util.List;

public class SingIn2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_in2);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        //spinner.setOnItemClickListener((AdapterView.OnItemClickListener) this);

        List<String> kodeNumber = new ArrayList<>();
        kodeNumber.add("+62");
        kodeNumber.add("+39");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

    }
}